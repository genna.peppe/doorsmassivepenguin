/** Copyright © 2021 by Giuseppe Genna. All rights reserved. */
/** This code can be downloaded just for testing of the functionality, any use of it in commercial or private project is prohibited */

This module is used to create simple doors in Unreal.
Both of Rotational or Moving are supported.

Steps:
- Select the door Mesh or Meshes
- Put them on a Pivot object if they should rotate and place the mesh on the pivot
- Define the mechanic and its DataAsset and add it to the Door object
- Define which event will trigger the door open or close ( or toggle )
- Voilà!

Videos in Resources directory

https://gitlab.com/genna.peppe/doorsmassivepenguin/-/blob/master/Resources/%231-%20Sliding%20Doors.webm
https://gitlab.com/genna.peppe/doorsmassivepenguin/-/blob/master/Resources/%232-%20HingeDoors.webm
